#!/usr/bin/env python3

import argparse
import gitlab
from datetime import datetime
import csv
import os

# consider activity in the last 180 days

parser = argparse.ArgumentParser(description='Deactivate all inactive users')
parser.add_argument('gitlaburl', help='Url of the gitlab instance')
parser.add_argument('token', help='API token able to read the requested projects')
args = parser.parse_args()

gitlaburl = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

try:
    users = gl.users.list(as_list=False)
except Exception as e:
    print("Can not retrieve list of users: "+ str(e))
    exit(1)

tz = datetime.now().astimezone().tzinfo

deactivated = []

for user in users:
    if user.attributes["state"] != "active":
        continue
    if "last_sign_in_at" not in user.attributes:
        print("Cannot access user field 'last_sign_in_at', use an admin token.")
        exit(1)
    else:
        if user.attributes["username"] not in ["ghost", "support-bot", "alert-bot"]:
            last_sign_in = user.attributes["last_sign_in_at"]
            last_activity = user.attributes["last_activity_on"]
            deactivate = True
            if last_sign_in is not None and last_activity is not None:
                last_sign_in_dt = datetime.strptime(last_sign_in, '%Y-%m-%dT%H:%M:%S.%f%z')
                last_activity_dt = datetime.strptime(last_activity, '%Y-%m-%d')
                delta_sign_in = datetime.now(tz) - last_sign_in_dt
                delta_activity = datetime.now() - last_activity_dt
                if delta_sign_in.days < 180 and delta_activity.days < 180:
                    deactivate = False
            if deactivate:
                try:
                    print("Deactivating %s" % user.attributes["username"])
                    user.deactivate()
                    deactivated.append(user.attributes)
                except Exception as e:
                    print("Could not deactivate %s: %s" % (user.attributes["username"], str(e)))

print("Deactivated %s users, writing report." % str(len(deactivated)))
reportfilepath = "report/deactivated_users_%s.csv" % str(datetime.now().date())
os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

with open(reportfilepath, "w") as reportfile:
    reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["username","name","email","last_sign_in_at"]
    reportwriter.writerow(fields)
    for user in deactivated:
        row = []
        for field in fields:
            row.append(user[field])
        reportwriter.writerow(row)

